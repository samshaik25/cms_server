const chai=require("chai")
const chaiHttp=require("chai-http");
const { response } = require("express");

const server= require("../server")


// Assertion style
chai.should();

chai.use(chaiHttp);

describe('Register and Login',()=>{

    it("it should Register user, Login user",(done)=>{
        chai.request(server)
        .post('/users/signup')
        .send({
            "name":"sameer",
            "email":"sameer@",
            "password":"sameer@"
        })
        .end((err,response)=>{
            response.should.have.status(200);
            response.body.message.should.equal("success");
        
                 // Follow up with Login 

            chai.request(server)
            .post('/users/login')
            .send({
                "email":"sameer@",
                "password":"sameer@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token
                done();
                })         
        })
    })
})

    describe("GET /users/getusers",()=>{
        it("should GET all users",(done)=>{
            chai.request(server)
            .get("/users/getusers")
            .end((err,response)=>{
                response.should.have.status(200);
                response.body.should.be.a('array');
                done()
            })
        })
    })


    describe(" GET /users/getusersinfo",()=>{
        it("should GET all USERSINFO",(done)=>{
            chai.request(server)
            .get("/users/getusersinfo")
            .end((err,response)=>{
                response.should.have.status(200);
                response.body.should.be.a('array');
                done()
            })
        })
    })
      

    
    describe(" GET /users/getuserbyid/:id",()=>{
  
        it("should GET a USER by ID",(done)=>{
            const id=8
            chai.request(server)
            .get("/users/getuserbyid/"+id)
            .end((err,response)=>{
                response.should.have.status(200);
                response.body.should.be.a('object');
                response.body.should.have.property('id')
                response.body.should.have.property('name')
                response.body.should.have.property('email')
                response.body.should.have.property('password')
        
                done()
            })
        })
       
        it("should NOT GET a USER by ID (invalid id)",(done)=>{
            const id=122
            chai.request(server)
            .get("/users/getuserbyid/"+id)
            .end((err,response)=>{
                response.should.have.status(404);
                response.body.message.should.equal("Invalid ID");
                done()
            })
        })  
    })



    describe(" GET users/getuserinfobyid/:id",()=>{ 
        it("should GET a USERINFO by ID",(done)=>{
            const id=6
            chai.request(server)
            .get("/users/getuserinfobyid/"+id)
            .end((err,response)=>{
                response.should.have.status(200);
                response.body.should.be.a('object');
                response.body.should.have.property('id')
                response.body.should.have.property('title')
                response.body.should.have.property('description')
                response.body.should.have.property('Userid')
        
                done()
            })
        })
        
        it("should NOT GET  USERINFO by ID (invalid id)",(done)=>{
            const id=122
            chai.request(server)
            .get("/users/getuserinfobyid/"+id)
            .end((err,response)=>{
                response.should.have.status(404);
                response.body.message.should.equal("Invalid ID");
                done()
            })
        })
    })

    
   
    describe("PUT users/updateuser",()=>{
        it("should login and UPDATE USER",(done)=>{

            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token
                            
              chai.request(server)
              .put('/users/updateuser')
              .send({
                  "id":8,
                  "name":"shaik sam",
                  "email":"shaik@",
                  "password":"shaik@"
              })
              .set('authorization','bearer '+token)
              .end((err,response)=>{
                   response.should.have.status(200)
                   response.body.message.should.equal("updated successfully")
                    done()
              })
           })
        })
    })



    describe("PUT users/updateuser",()=>{
        it("should NOT UPDATE USER (invalid ID)",(done)=>{

            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token
                    
              chai.request(server)
              .put('/users/updateuser')
              .send({
                  "id":100,
                  "name":"shaik sam",
                  "email":"shaik@",
                  "password":"shaik@"
              })
              .set('authorization','bearer '+token)
              .end((err,response)=>{
                   response.should.have.status(404)
                   response.body.message.should.equal("Invalid ID")
                    done()
              })
           })
        })
    })



    describe("PUT users/updateuserinfo",()=>{
        it("should login and UPDATE USERINFO",(done)=>{

            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token
            
              chai.request(server)
              .put('/users/updateuserinfo')
              .send({
                  "id":6,
                  "title":"songss",
                  "description":"my fav",    
              })
              .set('authorization','bearer '+token)
              .end((err,response)=>{
                   response.should.have.status(200)
                   response.body.message.should.equal("updated successfully")
                    done()
              })
           })
        })
    })




    describe("PUT users/updateuserinfo",()=>{
        it("should NOT UPDATE USERINFO (invalid ID)",(done)=>{

            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token
                            

              chai.request(server)
              .put('/users/updateuserinfo')
              .send({
                  "id":100,
                  "title":"dddd",
                  "description":"my",
              })
              .set('authorization','bearer '+token)
              .end((err,response)=>{
                   response.should.have.status(404)
                   response.body.message.should.equal("Invalid ID")
                    done()
              })
           })
        })
    })


    describe("DELETE   users/deleteuser",()=>{
        it("should DELETE USER",(done)=>{
            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token

                chai.request(server)
                .delete('/users/deleteuser')
                .send({
                    "id":70
                })
                .set('authorization','bearer '+token)
                .end((err,response)=>{
                    response.should.has.status(200);
                    response.body.message.should.equal("deleted succesfully")
                    done()
                })
            })
        })
    })




    describe("DELETE   users/deleteuser",()=>{
        it("should NOT DELETE USER (INVALID ID)",(done)=>{
            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token

                chai.request(server)
                .delete('/users/deleteuser')
                .send({
                    "id":100
                })
                .set('authorization','bearer '+token)
                .end((err,response)=>{
                    response.should.has.status(404);
                    response.body.message.should.equal("Invalid ID")
                    done()
                })
            })
        })
    })



    describe("POST users/postuserinfo",()=>{
        it("should login and post userinfo",(done)=>{
            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token
             
                chai.request(server)
                .post('/users/postuserinfo')
                .send({
                    "title":"gym",
                    "description":"sam",
                    "Userid":8
                })
                .set('authorization','bearer '+token)
                .end((err,response)=>{
                    response.should.have.status(200)
                    done()
                })


            })
        })

    })



    describe("DELETE user/deleteuserinfo",()=>{
        it("should DELETE USERINFO",(done)=>{
            chai.request(server)
            .post('/users/login')
            .send({
                "email":"shaik@",
                "password":"shaik@"
            })
            .end((err,response)=>{
                response.body.should.have.property('token');
                var token=response.body.token

                chai.request(server)
                .delete('/users/deleteuserinfo')
                .send({
                    "id":10
                })
                .set('authorization','bearer '+token)
                .end((err,response)=>{
                    response.should.has.status(200);
                    response.body.message.should.equal("deleted succesfully")
                    done()
                })
            })
        })
    })