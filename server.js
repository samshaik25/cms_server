const express = require("express");
const cors = require("cors");

require("dotenv").config();

const app = express();

app.use(cors());

const userRouter = require("./router/user.router");
app.use(express.json());
app.use("/users", userRouter);

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`listening on port ${port} `);
});

module.exports = app;
