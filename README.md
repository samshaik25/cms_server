**CREATING TABLE User**


CREATE TABLE User ( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,name VARCHAR(30) NOT NULL, email VARCHAR(50) NOT NULL ,password VARCHAR(100) NOT NULL) ENGINE = InnoDB



| id | name    | email   | password                                                    |


|  8 | sameer  | sam@    | $2b$10$NvvHLQKiULERSigaFfliIu7IrysAfzGzd3ku1jJZdyTojx.C.7Yse |


**CREATING TABLE UserInfo**


CREATE TABLE UserInfo(id INT(6) AUTO_INCREMENT PRIMARY KEY, title VARCHAR(60) NOT NULL, description VARCHAR(200) NOT NULL,Userid INT(6) UNSIGNED NOT NULL,CONSTRAINT `fk_UserInfo_User` FOREIGN KEY (Userid) REFERENCES User(id) ON DELETE CASCADE ON UPDATE RESTRICT ) ENGINE = InnoDB;


| id | title | description                                       | Userid |


|  6 | GYM   | Someone busier than you is working out right now. |      8 |


