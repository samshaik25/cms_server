const {
  create,
  getUsers,
  getUserById,
  UpdateUser,
  deleteUser,
  getUserByEmail,
  signup,
  createUserInfo,
  getAllUInfo,
  getUserInfoById,
  UpdateUserInfo,
  deleteUInfo,
  getInfoByUserId,
} = require("../model/user.model");

const { genSaltSync, hashSync, compareSync } = require("bcrypt");

const { sign } = require("jsonwebtoken");

const Redis = require("redis");
const redisClient = Redis.createClient();
const DEFAULT_EXPIRATION = 60;

module.exports = {
  createUser: (req, res) => {
    const body = req.body;
    console.log(body);

    const salt = genSaltSync(10);
    body.password = hashSync(body.password, salt);
    create(body, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred while creating user.",
        });
      }
      return res.send(results);
    });
  },
  getUsers: (req, res) => {
    redisClient.get("users", (err, users) => {
      if (err) console.log(err);
      if (users != null) {
        // console.log(JSON.parse(users))
        return res.send(JSON.parse(users));
      } else {
        getUsers((err, results) => {
          if (err) {
            return res.status(500).json({
              message:
                err.message || "Some error occurred while getting users.",
            });
          }

          redisClient.setex(
            "users",
            DEFAULT_EXPIRATION,
            JSON.stringify(results)
          );
          res.send(results);
        });
      }
    });
  },

  getUserById: (req, res) => {
    const id = req.params.id;
    redisClient.get(id, (err, users) => {
      if (err) console.log(err);
      if (users != null) {
        return res.send(JSON.parse(users));
      } else {
        getUserById(id, (err, results) => {
          if (err) {
            return res.status(500).json({
              message: err.message || "Some error occurred.",
            });
          }
          if (!results) {
            return res.status(404).send({
              message: "Invalid ID",
            });
          } else {
            redisClient.setex(id, DEFAULT_EXPIRATION, JSON.stringify(results));
            res.send(results);
          }
        });
      }
    });
  },

  UpdateUser: (req, res) => {
    const body = req.body;
    const salt = genSaltSync(10);
    body.password = hashSync(body.password, salt);
    UpdateUser(body, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred while updating.",
        });
      }
      if (!results) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      } else {
        res.send({
          message: "updated successfully",
        });
      }
    });
  },

  deleteUser: (req, res) => {
    const body = req.body;
    deleteUser(body, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred while deleting",
        });
      }
      if (!results) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      } else {
        res.send({
          message: "updated successfully",
        });
      }
    });
  },

  login: (req, res) => {
    const body = req.body;
    getUserByEmail(body.email, (err, results) => {
      if (err) {
        console.log(err);
      }
      if (!results) {
        return res.json({
          data: "invalid email",
        });
      }

      const result = compareSync(body.password, results.password);
      if (result) {
        results.password = undefined;
        const jsontoken = sign({ result: results }, "qwerty", {
          expiresIn: "24h",
        });
        return res.send({
          token: jsontoken,
        });
      } else {
        return res.json({
          message: "invalid email or password",
        });
      }
    });
  },

  signup: (req, res) => {
    const body = req.body;

    const salt = genSaltSync(10);
    body.password = hashSync(body.password, salt);
    signup(body, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred",
        });
      }
      return res.send({
        message: "success",
      });
    });
  },

  createInfo: (req, res) => {
    const body = req.body;
    createUserInfo(body, (err, results) => {
      if (err) {
        return res.status(500).json({
          message:
            err.message || "Some error occurred while creating the info.",
        });
      }
      return res.send(results);
    });
  },

  getAllUsersInfo: (req, res) => {
    redisClient.get("userinfo", (err, users) => {
      if (err) console.log(err);
      if (users != null) {
        return res.send(JSON.parse(users));
      } else {
        getAllUInfo((err, results) => {
          if (err) {
            return res.status(500).json({
              message:
                err.message || "Some error occurred while getting users.",
            });
          }
          redisClient.setex(
            "userinfo",
            DEFAULT_EXPIRATION,
            JSON.stringify(results)
          );

          res.send(results);
        });
      }
    });
  },

  getUserInfoById: (req, res) => {
    const id = req.params.id;
    getUserInfoById(id, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred.",
        });
      }
      if (!results) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send(results);
    });
  },

  UpdateUserInfo: (req, res) => {
    const body = req.body;

    UpdateUserInfo(body, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred while updating.",
        });
      }
      if (!results) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send({
        message: "updated successfully",
      });
    });
  },

  deleteUserInfo: (req, res) => {
    const body = req.body;
    deleteUInfo(body, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred while deleting",
        });
      }
      if (!results) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      }

      return res.send({
        message: "deleted succesfully",
      });
    });
  },

  getInfoByUserId: (req, res) => {
    const id = req.params.id;
    getInfoByUserId(id, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: err.message || "Some error occurred.",
        });
      }
      if (!results) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send(results);
    });
  },
};
