const router = require("express").Router();

const {
  createUser,
  getUsers,
  getUserById,
  UpdateUser,
  deleteUser,
  login,
  signup,
  createInfo,
  getAllUsersInfo,
  getUserInfoById,
  UpdateUserInfo,
  deleteUserInfo,
  getInfoByUserId,
} = require("../controller/user.controller");

const { checkToken } = require("../authentication/token.validation");

router.post("/postuser", checkToken, createUser);

router.post("/postuserinfo", checkToken, createInfo);

router.get("/getusers", getUsers);

router.get("/getusersinfo", getAllUsersInfo);

router.get("/getuserinfobyid/:id", getUserInfoById);

router.get("/getuserbyid/:id", getUserById);

router.put("/updateuser", checkToken, UpdateUser);

router.put("/updateuserinfo", checkToken, UpdateUserInfo);

router.delete("/deleteuser", checkToken, deleteUser);

router.delete("/deleteuserinfo", checkToken, deleteUserInfo);

router.post("/login", login);

router.post("/signup", signup);

router.get("/infoByUID/:id", getInfoByUserId);

module.exports = router;
